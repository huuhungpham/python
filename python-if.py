byte1 = 200
byte2 = 5
byte3 = 256
byte4 = 8

if byte1 <= 255 and byte2 <= 255 and byte3 <= 255 and byte4 <= 255:
    if byte1 == 10:
        print("de vier bytes vormen een IPv4-adres in het bereik 10.0.0.0/8")
    else:
        print("de vier byten vormen een IPv4-adres buiten het bereik 10.0.0.0/8")
else:
    print("de vier bytes vormen geen geldig IPv4-adres")
